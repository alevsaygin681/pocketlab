import React, {useState} from 'react';
import AppSettings from '@modules/AppSettings';
import {Redirect, withRouter} from 'react-router-native';

const UserAccountRedirect = ({history}) => {
  const [hasAccount, setHasAccount] = useState(null);
  AppSettings.users().then(accounts => {
    if (
      accounts.length === 0 &&
      history.location.pathname !== '/accounts/new'
    ) {
      history.replace('/accounts/new');
    }
  });
  return <></>;
};
export default withRouter(UserAccountRedirect);
