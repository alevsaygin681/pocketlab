import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, ScrollView, View, FlatList} from 'react-native';
import {
  List,
  Paragraph,
  Headline,
  Caption,
  ActivityIndicator,
  Avatar,
} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import GitLabAPI from '../../GitLabAPI';
import {withTitle} from '@components/Title';
import moment from 'moment';

const Projects = props => {
  const [projects, setProjects] = useState(null);
  props.setTitle('Projects');
  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab
      .projects({
        membership: gitlab.hasConnection(),
        order_by: 'last_activity_at',
        simple: true,
      })
      .then(response => {
        setProjects(response);
      })
      .catch(e => {
        console.log(e);
        setProjects([]);
      });
  }, []);

  if (projects === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }

  return (
    <FlatList
      data={projects}
      keyExtractor={item => item.id}
      renderItem={({item}) => {
        return (
          <List.Item
            title={`${item.name_with_namespace}`}
            description={item.description}
            onPress={() => props.history.push(`/projects/${item.id}/repo`)}
            left={props => {
              if (item.avatar_url !== null) {
                return (
                  <Avatar.Image size={25} source={{uri: item.avatar_url}} />
                );
              } else {
                return (
                  <Avatar.Text size={25} label={item.name[0]} color="white" />
                );
              }
            }}
          />
        );
      }}
    />
  );
};

export default withTitle(withRouter(Projects));
