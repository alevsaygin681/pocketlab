import React, {useState, useEffect} from 'react';
import {StyleSheet, FlatList, Text} from 'react-native';
import {
  Card,
  IconButton,
  withTheme,
  Divider,
  Modal,
  Portal,
  Paragraph,
  TextInput,
  List,
  Button,
} from 'react-native-paper';
import GitLabAPI from '../../../GitLabAPI';

const scopes = {
  users: {title: 'Assignee', attribute: 'name', unselected: 'Unassigned'},
  milestones: {
    title: 'Milestone',
    attribute: 'title',
    unselected: 'No Milestone',
  },
};

const SelectorModal = props => {
  const [query, setQuery] = useState('');
  const [results, setResults] = useState([]);
  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.searchProject(props.project, props.scope, query).then(response => {
      setResults(response);
    });
  }, [props.project, props.scope, query]);
  return (
    <Portal>
      <Modal visible={props.open} onDismiss={props.onClose}>
        <Card style={{margin: 10}}>
          <Card.Title
            title={`Select ${scopes[props.scope].title || 'Item'}`}
            right={iconProps => (
              <IconButton {...iconProps} icon="close" onPress={props.onClose} />
            )}
          />
          <Card.Content>
            <TextInput
              mode="outlined"
              label={`Search ${scopes[props.scope].title}s`}
              onChangeText={setQuery}
            />
            <Divider />
            <FlatList
              data={results}
              keyExtractor={item => item.id}
              renderItem={({item}) => (
                <List.Item
                  title={item[scopes[props.scope].attribute]}
                  onPress={() =>
                    props.onSelected(
                      item.id,
                      item[scopes[props.scope].attribute],
                    )
                  }
                />
              )}
            />
          </Card.Content>
        </Card>
      </Modal>
    </Portal>
  );
};

export default SelectorModal;
