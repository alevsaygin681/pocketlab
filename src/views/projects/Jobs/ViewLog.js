import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, ScrollView, View, FlatList} from 'react-native';
import {
  List,
  Paragraph,
  Headline,
  Caption,
  ActivityIndicator,
  Avatar,
  Divider,
} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import SyntaxHighlighter from 'react-native-syntax-highlighter';
import {atomOneLight} from 'react-syntax-highlighter/styles/hljs';
import GitLabAPI from '../../../GitLabAPI';
import stripAnsi from 'strip-ansi';

const ViewLog = props => {
  const {project, job} = props.match.params;
  const [content, setContent] = useState(null);

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.jobLog(project, job).then(response => {
      if (response !== false) {
        setContent(stripAnsi(response));
      }
    });
  }, [project, job]);

  if (content === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }

  return (
    <SyntaxHighlighter language="shell" style={atomOneLight}>
      {content}
    </SyntaxHighlighter>
  );
};

export default ViewLog;
