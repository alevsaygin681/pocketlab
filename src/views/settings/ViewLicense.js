import React, {useState, useEffect} from 'react';
import {ScrollView, StyleSheet, FlatList} from 'react-native';
import {withTheme, Headline, Caption} from 'react-native-paper';
import {useHistory, useRouteMatch} from 'react-router-native';
import {withTitle} from '@components/Title';
import Markdown from 'react-native-markdown-renderer';

const ViewLicense = props => {
  const [licenses, setLicenses] = useState({});
  const history = useHistory();

  useEffect(() => {
    import('../../../licenses.json').then(setLicenses);
  }, []);

  const match = useRouteMatch('/settings/license/:id');
  const packageName = decodeURIComponent(match.params.id);
  props.setTitle(packageName);

  if (licenses[packageName] === undefined) {
    return <></>;
  }

  return (
    <ScrollView>
      <Markdown>{licenses[packageName].licenseText}</Markdown>
    </ScrollView>
  );
};

export default withTitle(ViewLicense);
